import scrapy
import hashlib
import re



class DomainSpider(scrapy.Spider): #scrapy.spiders.SitemapSpider
    name = "domain"
    domain = None
    organisation = None
    custom_settings = {

    }

    def start_requests(self):
        #TODO make three options; regular url endpoint check, or scrapyd endpoint, of nothing (flush_all)
        self.logger.info(self.settings.copy_to_dict())
        self.logger.info(self.custom_settings)
        if not self.domain:
            self.logger.info("No domain argument passed")
            return []
        if not self.organisation:
            self.logger.info("No organisation argument passed")
            return []
        self.allowed_domains = [self.domain]
#        self.sitemap_urls = [f"https://{self.domain}/sitemap.xml"]

        self.start_urls = [f"https://{self.domain}/"]
        return super().start_requests()



    def parse(self, response):
        if response.status == 200:
            contenttype = response.headers.get("Content-Type").decode()
            if contenttype.startswith('text/html'):
                url = response.url
                title = response.css("title::text").getall()[-1]
                nonce_pattern = r"nonce=\".*?\""
                bodytext = re.sub(nonce_pattern,"nonce=\"dummy\"", response.text, flags=re.IGNORECASE)
                md5 = hashlib.md5(bodytext.encode('utf-8')).hexdigest()
                yield {"url": url,"title": title,"md5":md5}

                for href in response.xpath("//a/@href").getall():
                    fullhref = response.urljoin(href)
                    if fullhref.startswith("http"):
                        yield scrapy.Request(fullhref, self.parse)
