# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import logging

import json
import os
import hashlib
import http.client
from pathlib import Path
from urllib.parse import quote,urlparse

import scrapy
from itemadapter import ItemAdapter
from scrapy.http.request import NO_CALLBACK
from scrapy.utils.defer import maybe_deferred_to_future

from twisted.internet import reactor
from twisted.web.client import Agent
from twisted.web.http_headers import Headers


from twisted.internet.defer import succeed
from twisted.web.iweb import IBodyProducer

class WebhookPipeline:



    async def process_item(self, item, spider):
        if spider.name == "domain" and spider.domain:

            d = ItemAdapter(item).asdict()
            url = d['url']
            old_d = self.history.pop(url,None)
            
            for wh in self.webhooks:
                event = None
                if wh['mode'] == 'full' or not old_d: event = 'add'
                elif d['md5'] != old_d['md5']: event = 'update'
                if event:
                    d['event'] = event
                    d['organisation'] = spider.organisation
                    d['domain'] = spider.domain
                    if not wh['batch']:
                        request = scrapy.Request(wh['url'], method="POST", body=json.dumps(d),callback=NO_CALLBACK, headers={"Content-Type": "application/json"}) # headers here too
                        response = await maybe_deferred_to_future(
                            spider.crawler.engine.download(request)
                        )
                    else:
                        wh['result'].append(d)
            # d.pop('event',None)
            line = json.dumps(d) + "\n"
            self.file.write(line)

        return item

    def open_spider(self, spider):

            
        if spider.name == "domain" and spider.domain:
            urls = os.getenv('SCRAPY_WEBHOOKS')
            if urls:
                urls = urls.split(';')

            
            self.webhooks = []
            self.history = {}
            self.mode = "full"
            fp = Path(f"{spider.domain}.jsonl")
            if fp.is_file():
                self.mode = "delta"
                f = open(fp, "r")
                line = f.readline()
                while line:
                    d = json.loads(line)
                    url = d.get("url")
                    self.history[url] = d
                    line = f.readline()
                f.close()

            for url in urls:
                wh = {"url":url,"result":[]}
                self.webhooks.append(wh)
                
                parsed_url = urlparse(wh["url"])
                if (parsed_url[0] == 'http'):
                    wh["connection"] = http.client.HTTPConnection(parsed_url.hostname,parsed_url.port)
                elif (parsed_url[0] == 'https'):
                    wh["connection"] = http.client.HTTPSConnection(parsed_url.hostname,parsed_url.port)
                wh['parsed_url'] = parsed_url

                msg = {'organisation':spider.organisation,'domain':spider.domain, 'event':'start', 'mode': self.mode}

                response = self.http(wh,'POST',json.dumps(msg))
                mode = self.mode
                if self.mode == 'delta' and 'mode' in response:
                    mode = response['mode']
                batch = False
                if 'batch' in response:
                    batch = response['batch']
                wh['mode'] = mode
                wh['batch'] = batch
                wh['connection'].close()

            self.file = open(fp, "w")
        return

    def http(self, webhook, method, body):
        webhook['connection'].request(method, webhook['parsed_url'].path, headers={"Host": webhook['parsed_url'].hostname, "Content-Type": "application/json"}, body=body)
        response = webhook['connection'].getresponse()
        body = json.loads(response.read())

        logging.error(f"{response.status}, {response.reason}, {body}")
        return body

    # def twist(self, agent, url, method, body):
    #     logging.info(f"{method}: {type(method.encode('ascii'))}")
    #     logging.info(f"{url}: {type(url.encode('ascii'))}")
    #     d = agent.request(
    #         method.encode('ascii'),
    #         url.encode('ascii'),
    #         None,
    #         BytesProducer(body.encode('utf-8'))
    #         )
    #     d.addBoth(self._ignore)

    def _ignore(self, response):
        logging.info(response)
        pass


    def close_spider(self, spider):
        if spider.name == "domain" and spider.domain:
            self.file.close()
            #TODO DELETE OPERATION
            logging.info('close_spider')

            for wh in self.webhooks:
                parsed_url = urlparse(wh["url"])
                if (parsed_url[0] == 'http'):
                    wh["connection"] = http.client.HTTPConnection(parsed_url.hostname,parsed_url.port)
                elif (parsed_url[0] == 'https'):
                    wh["connection"] = http.client.HTTPSConnection(parsed_url.hostname,parsed_url.port)
                wh['parsed_url'] = parsed_url


            for k,e in self.history.items():
                for wh in self.webhooks:
                    if not wh['mode'] == 'full':
                        e['event'] = 'delete'
                        if not wh['batch']:
                            self.http(wh,'POST',json.dumps(e))
                        else:
                            wh['result'].append(e)
            self.history = {}
            for wh in self.webhooks:
                if wh['batch']:
                    self.http(wh,'POST',json.dumps({'organisation':spider.organisation,'domain':spider.domain, 'event':'batch','items':wh['result']}))
                msg = {'organisation':spider.organisation,'domain':spider.domain, 'event':'end'}
                self.http(wh,'POST',json.dumps(msg))
                wh['connection'].close()
        return

# class ScreenshotPipeline:
#     """Pipeline that uses Splash to render screenshot of
#     every Scrapy item."""

#     SPLASH_URL = "http://localhost:8050/render.png?url={}"

#     async def process_item(self, item, spider):
#         adapter = ItemAdapter(item)
#         encoded_item_url = quote(adapter["url"])
#         screenshot_url = self.SPLASH_URL.format(encoded_item_url)
#         request = scrapy.Request(screenshot_url, callback=NO_CALLBACK)
#         response = await maybe_deferred_to_future(
#             spider.crawler.engine.download(request)
#         )

#         if response.status != 200:
#             # Error happened, return item.
#             return item

#         # Save screenshot to file, filename will be hash of url.
#         url = adapter["url"]
#         url_hash = hashlib.md5(url.encode("utf8")).hexdigest()
#         filename = f"{url_hash}.png"
#         Path(filename).write_bytes(response.body)

#         # Store filename in item.
#         adapter["screenshot_filename"] = filename
#         return item