#!/usr/bin/python3
import http.client
import json
import urllib.parse

data = [
    {
        "organisation_slug": "meierijstad",
        "domains":[
            {
                "domain":"www.meierijstad.nl",
                "environment":"production",
                "index": True,
            }
        ]
    }
]

connection = http.client.HTTPConnection("scrapyd",6800)
for organisation in data:
    slug = organisation["organisation_slug"]
    domains = organisation["domains"]
    for d in domains:
        domain = d["domain"]
        environment = d.get("environment","staging")
        index = d.get("index", False)
        if index and environment == "production":
            print(f"adding index job: {slug}, {domain} ")
            
            # Define the form parameters (the data you want to send)
            form_data = {
                'project': 'default',
                'spider': 'domain',
                'domain': domain,
                'organisation': slug
            }
            # Encode the form data
            encoded_data = urllib.parse.urlencode(form_data)

            url = "/schedule.json"
            connection.request("POST", url, headers={"Host": "scrapyd", "Content-Type": "application/x-www-form-urlencoded"}, body=encoded_data)
            response = connection.getresponse()
            body = json.loads(response.read())

            print(f"{response.status}, {response.reason}, {body}")
connection.close()
