from sanic import Blueprint, Sanic, response, exceptions
from sanic.request import Request
from sanic.response import HTTPResponse, json
from auth import protected
from abc import ABC, abstractmethod
import datetime# import redis
import logging

logger = logging.getLogger(__name__)
class Reminder():
    def __init__(self, session_manager = None, sender_id:str = None, name:str = None, intent:str = None, entities:dict = None, output_channel:str = None, schedule = None):
        if isinstance(schedule,float):
            self.schedule = datetime.datetime.fromtimestamp(schedule)
        elif isinstance(schedule,str):
            self.latestactivity = datetime.datetime.fromisoformat(schedule)
        else:
            self.schedule = schedule

        self.sender_id = sender_id #
        self.name = name
        self.intent = intent
        self.entities = entities
        self.output_channel = output_channel #

class ReminderManager(ABC):
    reminder_manager = None

    def create_reminder(self,sender_id, name, **kwargs):
        logger.info("Create reminder from ReminderManager")
        reminder = Reminder(reminder_manager = self,sender_id=sender_id,name=name,session_manager = self, **kwargs)
        return reminder

    def to_dict(self, guid = None):
        reminders = self.get_reminders()
        now = datetime.datetime.now()
        result = {}
        reminderlist = []
        for sender_id,name in reminders:
            reminder = self.get_reminder(sender_id,name)
            rdict = reminder.to_dict()
            reminderlist.append(rdict)
        result['reminders'] = reminderlist
        return result


    @abstractmethod
    def get_reminders(self):
        pass

    @abstractmethod
    def get_reminder(self,sender_id, name):
        pass

    @abstractmethod
    def update_reminder(self,reminder):
        pass

    @abstractmethod
    def delete_reminder(self,reminder):
        pass

    @classmethod
    def get_reminder_manager(cls):
        pass
        if not cls.reminder_manager:
            if True: #not REDIS_URL:
                cls.reminder_manager = SimpleReminderManager()
            else:
                cls.reminder_manager = RedisReminderManager()
        return cls.reminder_manager
        
class SimpleReminderManager(ReminderManager):
    def __init__(self):
        self.reminders = {}

    def get_reminder(self,sender_id,name):
        reminder = self.sessions.get((sender_id,name))
        return reminder

    def get_reminders(self):
        return self.reminders.keys()

    def update_reminder(self,reminder):
        self.sessions[(reminder.sender_id, reminder.name)] = reminder
        
    def delete_reminder(self,reminder):
        return self.reminders.pop((reminder.sender_id, reminder.name), None)



# class RedisReminderManager(ReminderManager):

#     def __init__(self):
#         self.sessions = {} #prevent new instances, always check redis for existence
#         self.sessiondata = Redis.from_url(url = REDIS_URL, decode_responses=True)

#     def get_sessions(self):
#         sids = []
#         p_sids = self.sessiondata.scan()
#         sids.extend(p_sids[1])
#         while p_sids[0]>0:
#             p_sids = self.sessiondata.scan(p_sids[0])
#             sids.extend(p_sids[1])
#         if RedisSessionManager.CALLBACK_QUEUE in sids:
#             sids.remove(RedisSessionManager.CALLBACK_QUEUE)
#         return sids

#     def get_session(self,sender_id):
#         sessionstr = self.sessiondata.get(sender_id)
#         session = self.sessions.get(sender_id) if sessionstr else None
#         if sessionstr and not session: #only when it is available in redis, no autocreate
#             logger.info("restoring session from redis")
#             data = json.loads(sessionstr)
#             guid = data.pop('guid')
#             data.pop('sender_id')
#             # if 'log' in data:
#             #     data.pop('log')
#             session = self.create_session(guid,sender_id,**data) 
#             self.sessions[sender_id] = session
#         else:
#             logger.info("reusing session object from memory")

#         return session

#     def update_session(self,session):
#         self.sessions[session.sender_id] = session
#         self.sessiondata.set(session.sender_id, json.dumps(session.to_dict()))

    
#     def delete_session(self,session):
#         self.sessiondata.delete(session.sender_id)
#         self.sessions.pop(session.sender_id, None)



apiv1 = Blueprint("api_v1",version=1, version_prefix="/api/v")

@apiv1.route("/subjects")
async def subjects(request):
    return json([
        {"value":"--", "title": "Overig"},
        {"value":"wmo-voorziening", "title": "WMO"},
        {"value":"woz-beschikking", "title": "WOZ beschikking"},
        {"value":"woz-taxatieverslag", "title": "WOZ taxatieverslag"}]
    )   

@apiv1.get("/statistics")
async def get_statistics(request,token):
    return json(statistics)

@apiv1.get("/statistics/<organisation>")
async def get_statistics_by_org(request, organisation,token):
    return json([d for d in statistics if d['organisation_id'] == organisation])

@apiv1.get("/statistics/<organisation>/<session_id>")
async def get_statistic(request, organisation, session_id,token):
    return json(next(item for item in statistics if item["organisation_id"] == organisation and item['session_id'] == session_id))

@apiv1.patch("/statistics/<organisation>/<session_id>")
async def update_statistic(request, organisation, session_id,token):
    stat = next((item for item in statistics if item["organisation_id"] == organisation and item['session_id'] == session_id),None)
    if not stat:
        stat = {"organisation_id":organisation,"session_id":session_id}
        statistics.append(stat)
    allowed_keys = [
        "organisation_id",
        "session_id",
        "subjects",
        "num_correct_answers",
        "num_correct_answers_after_one_correction",
        "num_correct_answers_after_two_corrections",
        "num_failed_corrections",
        "num_incorrect_responses",
        "citizen_properly_assisted",
        "critical_error",
        "different_language",
        "personal_question",
        "remarks",
    ]
    request_dict = request.json
    # merge_dict = {k:request_dict[k] for k in filter if k in request_dict}
    # merge_dict = {key: request_dict[key] for key in allowed_keys}
    stat.update(request_dict)
    #TODO update from body
    return json(stat)

