from functools import wraps

import jwt
from sanic import text,json


def check_token(request, organisation):
    if not request.token:
        return False

    try:
        jwt.decode(
            request.token, request.app.config.SECRET, algorithms=["HS256"]
        )
    except Exception:
        return False
    else:
        return True


def protected(wrapped):
    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            result = {"error":"You are unauthorized."}
            if request.token:
                try:
                    token = jwt.decode(
                        request.token, request.app.config.SECRET, algorithms=["HS256"], require=['exp','aud','sub','org'], audience=["statistics"]
                    )
                    result['token'] = token
                    if token['org'] == '*' or ('organisation' in kwargs and token['org'] == kwargs['organisation']):
                        kwargs['token'] = token
                        response = await f(request, *args, **kwargs)
                        return response
                except Exception as e:
                    result['exception'] =str(e)
            return json(result, 401)

        return decorated_function

    return decorator(wrapped)
