#!/usr/local/bin/python
import asyncio
import logging
import aiohttp
import itertools
import copy
from aiohttp import web
import os
from datetime import timezone
import datetime
# from apiv1 import apiv1
from sanic.log import logger
from slugify import slugify



import requests
from elasticapm.contrib.sanic import ElasticAPM
from sanic import Blueprint, Sanic, response, exceptions
from sanic.request import Request
from sanic.response import HTTPResponse, json, text
import jwt



sentry_dsn = os.getenv("ROUTER_SENTRY_DSN", None)
if sentry_dsn:
    import sentry_sdk
    from sentry_sdk.integrations.sanic import SanicIntegration
    from sentry_sdk.integrations.redis import RedisIntegration

    sentry_sdk.init(dsn=sentry_dsn, integrations=[SanicIntegration(), RedisIntegration()])


app = Sanic(name="webhook-stub")
app.config.SECRET = "KEEP_IT_SECRET_KEEP_IT_SAFE"

# app.blueprint(apiv1)

if os.getenv("ELASTIC_APM_SERVER_URL"):
    environment = os.getenv("ENVIRONMENT")
    app.config.update({
        "ELASTIC_APM_SERVER_URL": os.getenv("ELASTIC_APM_SERVER_URL"),
        "ELASTIC_APM_SECRET_TOKEN": os.getenv("ELASTIC_APM_SECRET_TOKEN"),
    })
    apm = ElasticAPM(app=app, config={"SERVICE_NAME": f"Environment {environment}"})


# @app.get("/login")
# async def do_login(request):

#     token = {'sub':'test@test.nl','aud':'statistics','exp':datetime.datetime.now(tz=timezone.utc) + datetime.timedelta(seconds=3600)}
#     if 'sub' in request.args:
#         token['sub'] = request.args['sub'][0]
#     # if 'uname' in request.args:
#     #     token['uname'] = request.args['uname'][0]
#     if 'org' in request.args:
#         token['org'] = request.args['org'][0]
#     enc = jwt.encode(token, request.app.config.SECRET)
#     token['token'] = enc
#     token.pop('exp')
#     return json(token)

async def generate(app,jobid): #async method
    running = getattr(app.ctx,f'{jobid}-state',False)
    if not running:
        return
    queue = getattr(app.ctx,f'{jobid}-queue')
    while running or len(queue) > 0:
        await asyncio.sleep(0.2) # simulate async operation
        if len(queue) > 0:
            yield queue.pop(0)
        else:
            await asyncio.sleep(0.5) # nothing to yield, wait a moment
        queue = getattr(app.ctx,f'{jobid}-queue')
        running = getattr(app.ctx,f'{jobid}-state',False)
     
async def index(batch):
    logger.info('start batch index')
    logger.info(f'batch = {batch}')
    asyncio.sleep(20)
    logger.info('end batch index')

async def processor(app,jobid):
    logger.info(f'start processing {jobid}')
    batch_size = 10
    batch = [] # is ook redis
    async for event in generate(app,jobid):
        logger.info(f'processing event: {event}')
        # do some preprocessing (pipeline)
        # download and transform to markdown
        # ....
        batch.append(event['url']) #only append url for demo purpose; write batch item to redis queue
        if len(batch) >= batch_size:
            await index(batch)
            batch = [] # reset batch
    # when loop finished, call latest batch
    await index(batch)
    batch = [] # reset batch
    #when finished processing loop, cleanup data
    delattr(app.ctx,f'{jobid}-state')
    delattr(app.ctx,f'{jobid}-queue')
    logger.info(f'end processing {jobid}')



@app.route("/", methods=["GET"])
async def health(_: Request) -> HTTPResponse:
    return response.json({"status": "ok"})

@app.route("/webhook", methods=["POST"])
async def webhook(request: Request) -> HTTPResponse:
    # logger.info(request.json)
    jobid = slugify(request.json['domain'])

    if request.json['event'] == 'start':
        logger.info(f'start job: {jobid}')
        # initialize queue
        setattr(app.ctx,f'{jobid}-state',True)
        setattr(app.ctx,f'{jobid}-queue',[])

        # start processing task before returning mode
        app.add_task(processor(app,jobid))

        return response.json({"status":"ok","batch":False,"mode":"full"})

    elif request.json['event'] == 'end':
        logger.info(f'end job: {jobid}')

        # set running state to false
        setattr(app.ctx,f'{jobid}-state',False)

    else: # assume scrape event
        # get relevant queue
        list = getattr(app.ctx,f'{jobid}-queue')
        # append item to list
        list.append(request.json)

    return response.json({"status": "ok"})

if __name__ == "__main__":
    app.run(host="0.0.0.0")
